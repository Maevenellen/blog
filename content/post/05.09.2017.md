---
title: Dinsdag 05.09.2017
subtitle: Communication Multimedia Design, Leerjaar 1
date: 2017-09-05
tags: [“05.09”, “FlexFive”, "hoorcollege", "dinsdag"]
---

- Om half 11 begon mijn hoorcollege. Dit hoorcollege ging over het ontwerpproces. Deze heb ik bijgehouden in mijn Dummy.
![ontwerpproces](1.JPG) ![ontwerpproces](2.JPG) ![ontwerpproces](3.JPG) ![ontwerpproces](4.JPG)
- Vandaag heb ik thuis ook nog verder gewerkt aan mijn Hotspot onderzoek, en ben ik aan de slag gegaan voor mijn moodboard naar een doelgroep. Deze heb ik ook afgemaakt uiteindelijk. Bij mijn visuele hotspot onderzoek heb ik nog een collage gemaakt van alle gebouwen. Deze voeg ik morgen in mijn blog toe, omdat we ze dan gaan bespreken.
![hotspots](Screen Shot 2017-09-10 at 15.06.03.png)