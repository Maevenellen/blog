---
title: Woensdag 06.09.2017
subtitle: Communication Multimedia Design, Leerjaar 1
date: 2017-09-06
tags: [“06.09”, “FlexFive”, "werkcollege", "woensdag"]
---
![planning](Screen Shot 2017-09-19 at 15.24.13.png)
- Vandaag werd ons in de ochtend gevraagt om de vier rollen: Voorzitter, natulist, timekeeper en cheerleader te verdelen onder ons 5. Het overgebleven persoon zou bijstaander zijn, dat ben ik uiteindelijk geworden omdat ik me eigenlijk wel in alles kwijt kan. Onder deze 4 namen moesten we voor onszelf noteren wat we belangrijk vonden als taken bij de rol. Uiteindelijk moest je ook kiezen voor welke rol je het liefst meer zou willen leren en onder controle zou willen hebben. Ik heb gekozen voor voorzitter, omdat ik graag wil leren om meer mijn mening te delen in de groep, en mijn stem meer te doen laten gelden. Ook lijkt het me interessant om meer de leiding te nemen soms!
![Rolverdeling](IMG_3031.JPG)
- Na de ochtend die vooral bestond uit groepsactiviteiten om elkaar beter te leren kennen, en elkaar beter te plaatsen in het team, hebben we eerst binnen ons groepje gedeeld hoe ons onderzoek is verlopen. We besproken welk thema en welke doelgroep we zouden kiezen. We hebben gekozen voor Leisure management, mijn moodboard is daarop gebaseerd, en een mix van al onze visuele onderzoeken qua thema's. Hierna gaven we elkaar ook feedback.
![hotspots collage](Hotspots Rotterdam.pdf) ![moodboard leisure management](vrijetijdsmanagement moodboard.pdf) ![info naast moodboard](Screen Shot 2017-09-19 at 15.23.01.png) ![feedback](Screen Shot 2017-09-19 at 15.23.07.png)
- Toen kregen we een blogworkshop. Hier heb ik aantekeningen bij gemaakt.
![Blog](IMG_3032.JPG) ![Blog](IMG_3033.JPG) 
- Na de blogworkshop hebben we nog even snel onze ideeen op tafel gegooid, omdat we deze nog niet echt concreet hadden gedeeld, en die ochtend onze onderzoeken hadden gedeeld. De ideeen heb ik ook opgeschreven. Dit is wat we tot nu toe hebben.
![spel](IMG_3034.JPG) 