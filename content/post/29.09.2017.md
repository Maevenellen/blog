---
title: Donderdag 29.09.2017
subtitle: Communication Multimedia Design, Leerjaar 1
date: 2017-09-29
tags: [“29.09”, “FlexFive”, "werkcollege", "vrijdag"]
---

- Ik heb vandaag gewerkt aan minigames voor ons spel. Zo heb ik plaatjes gemaakt voor 'Raad de Rotterdammer'
![iteratie](Screen Shot 2017-10-27 at 11.27.42.png)
- Ik heb muziek gedownload voor de minigame 'Raad de plaat'
![iteratie](Screen Shot 2017-10-27 at 11.27.27.png)